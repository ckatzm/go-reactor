package fileevents

import (
	"time"
)

type FileUpdatedEvent struct {
	filePath  string
	name      string
	data      map[string]interface{}
	eventTime time.Time
}

func (e FileUpdatedEvent) Name() string {
	return e.name
}
