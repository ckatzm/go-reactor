package reactions

import (
	"../events"
	"../reactor"
)

type Reaction interface {
	ReactTo(event events.Event)
}

type EmitEventDataReaction struct {
	outputFormat reactor.OutputFormat
}

func (e EmitEventDataReaction) ReactTo(event events.Event) {
	e.outputFormat.Send(event)
}

type RegenerateGraphReaction struct {
	// TODO
}