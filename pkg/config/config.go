package config

import (
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	ListeningPort    int      `json:"ListeningPort"`
	MongoAddress     string   `json:"MongoAddress"`
	MongoPort        int      `json:"MongoPort"`
	MongoDatabase    string   `json:"MongoDatabase"`
	MongoCollections []string `json:"MongoCollections"`
}

func BuildFromFile(fp string) (c Config) {
	f, err := ioutil.ReadFile(fp)
	if err != nil {
		panic(err)
	}

	if err := json.Unmarshal(f, &c); err != nil {
		panic(err)
	}

	return c
}
