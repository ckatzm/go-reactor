package serializers

import "../../events"

type EventSerializer interface {
	Serialize(event events.Event) []byte
}
