package serializers

import (
	"../collectionevents"
	"encoding/json"
)

func CollectionUpdatedJsonEventSerializer(e collectionevents.CollectionUpdatedEvent) []byte {
	payload := struct {
		EventType      string `json:"eventType"`
		EventTime      int64  `json:"eventTime"`
		CollectionName string `json:"collectionName"`
		Data           []byte `json:"data"`
	}{
		e.Type(),
		e.Time(),
		e.CollectionName(),
		e.Data(),
	}

	data, err := json.Marshal(&payload)
	if err != nil {
		panic(err)
	}
	return data
}
