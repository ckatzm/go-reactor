package eventchanneloutputformat

import (
	"../events"
)

type ChildEventOutputFormat struct {
	eventChan *chan events.Event
}

func New(eventChan *chan events.Event) *ChildEventOutputFormat {
	return &ChildEventOutputFormat {
		eventChan,
	}
}

func (o *ChildEventOutputFormat) Send(eventType string, data []byte) {
	*o.eventChan <- events.Event{
		eventType,
	}
}
