package websocketoutputformat

import (
	"github.com/gorilla/websocket"
	"log"
)

type WebSocketOutputFormat struct {
	connections *map[*websocket.Conn]bool
}

func New(connPool *map[*websocket.Conn]bool) WebSocketOutputFormat {
	return WebSocketOutputFormat{
		connPool,
	}
}

func (o WebSocketOutputFormat) Send(data []byte) {
	for connection := range *o.connections {
		err := connection.WriteMessage(websocket.TextMessage, data)
		if err != nil {
			log.Printf("Failed to send message: %s", err)
			connection.Close()
			delete(*o.connections, connection)
		}
	}
}
