package collectionevents

type CollectionUpdatedEvent struct {
	eventType      string
	collectionName string
	eventTime      int64
	documentData   []byte
}

func (e CollectionUpdatedEvent) Type() string {
	return e.eventType
}

func (e CollectionUpdatedEvent) Time() int64 {
	return e.eventTime
}

func (e CollectionUpdatedEvent) CollectionName() string {
	return e.collectionName
}

func (e CollectionUpdatedEvent) Data() []byte {
	return e.documentData
}
