package collectionevents

import (
	"../events"
	"context"
	"encoding/json"
	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/mongo"
	"github.com/mongodb/mongo-go-driver/mongo/options"
	"github.com/mongodb/mongo-go-driver/x/bsonx"
	"log"
	"time"
)

type CollectionWatcher struct {
	client     *mongo.Client
	db         *mongo.Database
	collection *mongo.Collection
	eventQueue chan events.Event
}

func NewWatcher(client *mongo.Client, dbName string, collectionName string, eventQueue chan events.Event) *CollectionWatcher {
	/* New returns a pointer to a new instance of a CollectionWatcher */
	db := client.Database(dbName)

	cw := CollectionWatcher{
		client,
		db,
		db.Collection(collectionName),
		eventQueue,
	}

	return &cw
}

func (c CollectionWatcher) SpwanEvent(eventData map[string]interface{}) {

	data, err := json.Marshal(&eventData)
	if data != nil {
		panic(err)
	}

	event := CollectionUpdatedEvent{
		"collectionUpdated",
		c.collection.Name(),
		time.Now().UnixNano(),
		data,
	}

	c.eventQueue <- &event
}

func (c CollectionWatcher) WatchInserts(returnKeys []string) {
	/*
		WatchInserts listens for inserts on the MongoDB ChangeStream
		It will look up each of the keys passed in on the document and add them to
		data property of the Event it spawns by calling SpawnEvent.
	*/

	ctx := context.Background()
	opts := options.ChangeStream()

	var fd options.FullDocument = "updateLookup"
	opts.FullDocument = &fd

	var pipeline mongo.Pipeline = mongo.Pipeline{{
		{"$match", bson.D{{"operationType", "insert"}}},
	}}

	watchCursor, err := c.collection.Watch(ctx, pipeline, opts)

	if err != nil {
		log.Printf("Error creating listener: %s", err)
		panic(err)
	}

	for watchCursor.Next(ctx) {
		doc := bsonx.Doc{}
		if err := watchCursor.Decode(doc); err != nil {
			log.Fatal("An error occurred decoding the document: %s", err)
		}

		eventData := make(map[string]interface{})
		// extract values from doc, add to eventData as key: value
		for _, key := range returnKeys {
			eventData[key] = doc.Lookup(key)
		}

		c.SpwanEvent(eventData)

	}

	if err := watchCursor.Err(); err != nil {
		log.Fatal(err)
	}

}
