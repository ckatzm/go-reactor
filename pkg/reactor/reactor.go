package reactor

import "../events"
import "../deserializers"
import "../reactions"

type Reactor struct {
	reactions     map[string]reactions.Reaction
	eventChannel  chan events.Event
	deserializers map[string]deserializers.Deserializer	// TODO: confirm this is not needed
}

/* New constructs a new Reactor */
func New(ch chan events.Event) Reactor {
	return Reactor{
		make(map[string]reactions.Reaction),
		ch,
		make(map[string]deserializers.Deserializer),
	}
}

/* RegisterReaction adds a reaction to the reactions map */
func (r Reactor) RegisterReaction(eventName string, rn reactions.Reaction ) {
	r.reactions[eventName] = rn
}

func (r Reactor) React() {
	for event := range r.eventChannel {
		//never close the event channel, this loop should run forever
		reaction := r.reactions[event.Type()]
		go reaction.ReactTo(event)
	}
}
