package reactor

import "github.com/gorilla/websocket"

type OutputFormat interface {
	Send()
}

type WebSocketOutputFormat struct {
	connections map[string]websocket.Conn
}

func New(conns map[string]websocket.Conn) WebSocketOutputFormat {

}
