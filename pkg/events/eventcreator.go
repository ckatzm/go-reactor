package events

type EventCreator interface {
	ConnectToEventChannel(chan Event)
	SpawnEvent(Event)
}
