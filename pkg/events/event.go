package events

type Event interface {
	Type() string
	Time() int64
	Data() []byte
}
