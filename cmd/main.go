package main

import (
	"../pkg/collectionevents"
	"../pkg/config"
	"../pkg/events"
	"../pkg/reactor"
	"context"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"github.com/mongodb/mongo-go-driver/mongo"
	"log"
	"net/http"
	"strconv"
)

var conf = config.BuildFromFile("../config.json")

var eventQueue = make(chan events.Event)

var connections = make(map[*websocket.Conn]bool)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func main() {

	// setup client for mongo watchers
	client, err := mongo.Connect(context.Background(),
		"mongodb://"+conf.MongoAddress+":"+strconv.Itoa(conf.MongoPort))
	if err != nil {
		panic(err)
	}

	// generate a watcher for each collection being watched
	for _, collName := range conf.MongoCollections {

		cw := collectionevents.NewWatcher(client, conf.MongoDatabase, collName, eventQueue)
		// generate events, extract data from documents at keys
		go cw.WatchInserts([]string{"positive", "neutral", "negative"})
	}

	r := reactor.New(eventQueue)

	r.RegisterReaction("collectionUpdated", func(i interface{}) {
		emitEventData(i.(events.Event).Data())
	})

	// handle incoming events
	go r.StartReacting()

	router := mux.NewRouter()

	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("../static/"))))

	router.HandleFunc("/ws", wsHandler)

	log.Fatal(http.ListenAndServe(":8228", router))
}

func wsHandler(w http.ResponseWriter, r *http.Request) {

	// upgrade to a websocket connection
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("An error occurred: %s", err)
		http.Error(w, "Bad Request.", http.StatusBadRequest)
	}
	// hold onto this connection in the global connections set
	connections[ws] = true
}

func emitEventData(e events.Event) {

}
